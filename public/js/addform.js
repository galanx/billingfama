
var DefaultName = "produkti";
var DefaultNameIncrementNumber = 0;

// No further customizations required.
function AddFormField(id,type,name,value,tag,element) {
if(! document.getElementById && document.createElement) { return; }
var inhere = document.getElementById(id);
var formfield = document.createElement(element);
var option = document.createElement("option");
option.value = "";
option.innerHTML = "--Zgjidheni Produktin--";

var sasiaLabel = document.createElement("Label");
var sasia = document.createElement("input");

var cmimiLabel = document.createElement("Label");
var cmimi = document.createElement("input");

formfield.appendChild(option);

var numri = document.getElementById("numri").value;
var createdNum = document.createElement("input");
createdNum.type="text";
createdNum.name="createdNum";
createdNum.id="createdNum";

var saHere = document.getElementById("saHere");

var list = new Array();
for(var i=0;i<numri;i++){
	list[i]=document.getElementById("list"+i).value;
	var optionList = document.createElement("option");
	optionList.innerHTML = list[i];
	optionList.value = $('#list'+i).attr('name');
	formfield.appendChild(optionList);
}

if(name.length < 1) {
   DefaultNameIncrementNumber++;
   saHere.value++;
   name = String(DefaultName + DefaultNameIncrementNumber);
   sasiaName = String("sasia" + DefaultNameIncrementNumber);
   cmimiName = String("cmimi" + DefaultNameIncrementNumber);
   createdNum.value += DefaultNameIncrementNumber;
   }
formfield.name = name;
formfield.id = name;
formfield.setAttribute("onmouseover", "pershkrimi(this.value)");
formfield.type = type;
formfield.value = value;
sasia.name = sasiaName;
sasia.id = sasiaName;
sasia.setAttribute("class","sasia");
sasia.type="text";
sasia.setAttribute("onkeypress","validate(event)");
sasia.setAttribute("style","width:50px;padding:0;margin:0;");
cmimi.name = cmimiName;
cmimi.id = cmimiName;
cmimi.setAttribute("class","cmimi");
cmimi.type="text";
cmimi.setAttribute("onkeypress","validate(event)");
cmimi.setAttribute("style","width:50px;padding:0;margin:0;");

if(tag.length > 0) {
   var thetag1 = document.createElement(tag);
   var thetag2 = document.createElement(tag);
   var thetag3 = document.createElement(tag);
   var thetag4 = document.createElement(tag);

   thetag1.appendChild(formfield);
   thetag2.id="pershkrimi"+String(DefaultNameIncrementNumber);
   thetag3.appendChild(sasia);
   thetag4.appendChild(cmimi);
   
   var trtag = document.createElement("tr");
   trtag.appendChild(thetag1);
   //trtag.appendChild(thetag2);
   trtag.appendChild(thetag3);
   trtag.appendChild(thetag4);

   inhere.appendChild(trtag); 
}
else { inhere.appendChild(formfield); }
} // function AddFormField()
