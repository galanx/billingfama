$(document).ready(function(){
//open popup
	$("#pop").click(function(){
		$("#overlay_form").fadeIn(1000);
		positionPopup();
	});
 	$("#shto_klient").click(function(){
		$("#shtoklient_form").fadeIn(1000);
		klientpositionPopup();
	});
//close popup
	$("#close").click(function(){
		$("#overlay_form").fadeOut(500);
	});

	$("#close_klient").click(function(){
		$("#shtoklient_form").fadeOut(500);
	});
	//open popup
	$("#popEdit").click(function(){
		$("#edit_form").fadeIn(1000);
		positionPopup();
	});
 
//close popup
	$("#closeEdit").click(function(){
		$("#edit_form").fadeOut(500);
	});
});
 
//position the popup at the center of the page
function positionPopup(){
	if(!$("#overlay_form").is(':visible')){
		return;
	}
	$("#overlay_form").css({
		left: ($(window).width() - $('#overlay_form').width()) / 2,
		top: ($(window).width() - $('#overlay_form').width()) / 7,
		position:'absolute',
	});
}

function editpositionPopup(){
	if(!$("#edit_form").is(':visible')){
		return;
	}
	$("#edit_form").css({
		left: ($(window).width() - $('#edit_form').width()) / 2,
		top: ($(window).width() - $('#edit_form').width()) / 7,
		position:'absolute',
	});
}
function klientpositionPopup(){
	if(!$("#shtoklient_form").is(':visible')){
		return;
	}
	$("#shtoklient_form").css({
		left: ($(window).width() - $('#shtoklient_form').width()) / 2,
		top: ($(window).width() - $('#shtoklient_form').width()) / 7,
		position:'absolute',
	});
}
//maintain the popup at center of the page when browser resized
$(window).bind('resize',positionPopup);
$(window).bind('resize',editpositionPopup);
$(window).bind('resize',klientpositionPopup);
 