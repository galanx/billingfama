<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Simply tell Laravel the HTTP verbs and URIs it should respond to. It is a
| breeze to setup your application using Laravel's RESTful routing and it
| is perfectly suited for building large applications and simple APIs.
|
| Let's respond to a simple GET request to http://example.com/hello:
|
|		Route::get('hello', function()
|		{
|			return 'Hello World!';
|		});
|
| You can even respond to more than one URI:
|
|		Route::post(array('hello', 'world'), function()
|		{
|			return 'Hello World!';
|		});
|
| It's easy to allow URI wildcards using (:num) or (:any):
|
|		Route::put('hello/(:any)', function($name)
|		{
|			return "Welcome, $name.";
|		});
|
*/

/*Route::get('/', function()
{
	return View::make('home.index');
});*/


//Home
Route::get("/login", array("as"=>"login","uses"=>"fama.login@index"));
Route::post("login/check", array("uses"=>"fama.login@authenticate"));
Route::get("logout", array("as"=>"logout", "uses"=>"fama.login@logout"));

Route::group(array("before"=>"auth"),function(){
	Route::get("/",array("as"=>"home","uses"=>"fama.home.index@index"));
	Route::get("/klientet",array("as"=>"klientet","uses"=>"fama.klientat.index@index"));
	Route::post("/shtoklient",array("uses"=>"fama.klientat.shtoklient@shtoklient"));
	Route::post("/fshijklient",array("uses"=>"fama.klientat.fshijklient@fshijklient"));
	Route::post("/kerkoklient",array("uses"=>"fama.klientat.kerkoklient@kerkoklient"));
	Route::get("/kaca",array("as"=>"kaca","uses"=>"fama.kaca.index@index"));
	Route::post("/kaca/paguaj",array("uses"=>"fama.kaca.index@paguaj"));
	Route::get("/shitja",array("as"=>"shitja","uses"=>"fama.shitja.index@index"));
	Route::post("/shitja/shtohyrje",array("uses"=>"fama.shitja.shto@shtohyrje"));
	Route::get("/sherbimet",array("as"=>"sherbimet","uses"=>"fama.sherbimet.index@index"));
	Route::post("/sherbimet/shto",array("uses"=>"fama.sherbimet.index@shto"));
	Route::post("/sherbimet/fshij",array("uses"=>"fama.sherbimet.index@fshij"));
	Route::get("/raportet",array("as"=>"raportet","uses"=>"fama.raportet.index@index"));
	Route::post("/raportet/view",array("uses"=>"fama.raportet.index@view"));
	Route::post("/saveevent", array("uses"=>"fama.home.saveevent@saveevent"));
	Route::post("/editevent", array("uses"=>"fama.home.editevent@editevent"));
	Route::get("/deleteevent/(:any)", array("uses"=>"fama.home.deleteevent@deleteevent"));
	Route::get("/konto",array("as"=>"konto","uses"=>"fama.konto.index@index"));
	Route::get("/konto/rolet",array("as"=>"konto_role","uses"=>"fama.konto.index@rolet"));
	Route::get("/konto/info",array("as"=>"konto_info","uses"=>"fama.konto.index@info"));
	Route::get("/konto/shto",array("as"=>"konto_shto","uses"=>"fama.konto.index@shto"));
	Route::get("/konto/edit/(:any)",array("as"=>"konto_edit","uses"=>"fama.konto.index@edit"));
	Route::get("/konto/shtorole",array("as"=>"role_shto","uses"=>"fama.konto.index@shtorole"));
	Route::get("/konto/ndryshorole/(:any)",array("as"=>"role_ndrysho","uses"=>"fama.konto.index@ndryshorole"));
	Route::post("/konto/infochange",array("uses"=>"fama.konto.index@infochange"));
	Route::post("/konto/shtokonto",array("uses"=>"fama.konto.index@shtokonto"));
	Route::post("/konto/editkonto",array("uses"=>"fama.konto.index@editkonto"));
	Route::post("/konto/fshijkonto",array("uses"=>"fama.konto.index@deletekonto"));
	Route::post("/konto/addrole",array("uses"=>"fama.konto.index@addrole"));
	Route::post("/konto/editrole",array("uses"=>"fama.konto.index@editrole"));
	Route::post("/konto/fshijrole",array("uses"=>"fama.konto.index@fshijrole"));
	Route::get("/produktet",array("as"=>"produktet","uses"=>"fama.produktet.index@index"));
	Route::get("/produktet/hyrjet",array("as"=>"produktet_hyrje","uses"=>"fama.produktet.index@hyrjet"));
	Route::post("/produktet/shtohyrje",array("uses"=>"fama.produktet.index@shtohyrje"));
	Route::post("/produktet/shto",array("uses"=>"fama.produktet.index@shto"));
	Route::post("/produktet/fshij",array("uses"=>"fama.produktet.index@fshij"));
	Route::post("/terminesearch",array("uses"=>"fama.home.search@search"));
});


Route::post("/events", function(){
	return View::make("fama.home.events");
});

Route::post("/terminesearch/(:any)",function($id){
	return View::make("fama.home.terminesearch")
	->with("puntori_id",$id);
});
Route::post("/changeevent", function(){
	return View::make("fama.home.changeevent");
});
Route::post("/ajax", function(){
	return View::make("fama.shitja.ajax");
});


/*
|--------------------------------------------------------------------------
| Application 404 & 500 Error Handlers
|--------------------------------------------------------------------------
|
| To centralize and simplify 404 handling, Laravel uses an awesome event
| system to retrieve the response. Feel free to modify this function to
| your tastes and the needs of your application.
|
| Similarly, we use an event to handle the display of 500 level errors
| within the application. These errors are fired when there is an
| uncaught exception thrown in the application.
|
*/

Event::listen('404', function()
{
	return Response::error('404');
});

Event::listen('500', function()
{
	return Response::error('500');
});

/*
|--------------------------------------------------------------------------
| Route Filters
|--------------------------------------------------------------------------
|
| Filters provide a convenient method for attaching functionality to your
| routes. The built-in before and after filters are called before and
| after every request to your application, and you may even create
| other filters that can be attached to individual routes.
|
| Let's walk through an example...
|
| First, define a filter:
|
|		Route::filter('filter', function()
|		{
|			return 'Filtered!';
|		});
|
| Next, attach the filter to a route:
|
|		Route::get('/', array('before' => 'filter', function()
|		{
|			return 'Hello World!';
|		}));
|
*/

Route::filter('before', function()
{
	// Do stuff before every request to your application...
});

Route::filter('after', function($response)
{
	// Do stuff after every request to your application...
});

Route::filter('csrf', function()
{
	if (Request::forged()) return Response::error('500');
});

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::to('login');
});