<?php
	class Fama_Login_Controller extends Base_Controller{
		public $layout = "fama.layouts.login";

		public function action_index(){
			$this->layout->content = View::make("fama.login");
		}

		public function action_authenticate(){
			$username = Input::get('username');
			$password = Input::get('password');
			$credentials =  array(
				'username'=> $username, 
				'password'=> $password,
			);
			
			$id = Users::where("username","=",$username)->first()?
			Users::where("username","=",$username)->first()->id:"";

			if(Auth::attempt($credentials)){
				if(Auth::login($id))

					return Redirect::to_route("home");
			}else{
				return Redirect::to_route("login")
				->with("msg","Të dhënat nuk janë të sakta!");
			}
		}

		public function action_logout(){
			Auth::logout();

			return Redirect::to_route("login");
		}
	}
?>