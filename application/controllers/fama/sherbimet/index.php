<?php
	class Fama_Sherbimet_Index_Controller extends Fama_Controller{

		public function action_index(){
			$this->layout->content = View::make("fama.sherbimet.index");
		}

		public function action_shto(){
			$sherbimi = Input::get("sherbimi");
			$cmimi = Input::get("cmimi");

			if($sherbimi==""||$cmimi==""){
				return Redirect::back();
			}
				$shto = Sherbimet::create(array(
					"sherbimi"=>$sherbimi,
					"cmimi"=>$cmimi,
				));

			if($shto){
				return Redirect::back()->with("msg","Sherbimi u shtua me sukses!");
			}else{
				return Redirect::back()->with("error","Sherbimi nuk u shtua!");
			}
		}

		public function action_fshij(){
			$checked = Input::get("checked");

			if($checked==""){
				return Redirect::back();
			}

			foreach($checked as $check){
				$delete = Sherbimet::where("id","=",$check)->delete();
			}

			return Redirect::back();
		}
	}
?>