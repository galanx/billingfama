<?php
	class Fama_Home_Editevent_Controller extends Fama_Controller{

		public function action_editevent(){
			$id = Input::get("id");
			$titulli = Input::get('titulliEdit');
			$data = Input::get('dataEdit');
			$prej = Input::get('prejEdit');
			$deri = Input::get('deriEdit');
			$pershkrimi = Input::get('pershkrimiEdit');
			$puntori = Input::get("puntoriEdit");

			$event = Events::where("id","=",$id)->first();
			$event->title = $titulli;
			$event->start = date("Y-m-d ".$prej.":s",strtotime($data));
			$event->end = date("Y-m-d ".$deri.":s",strtotime($data));
			$event->description = $pershkrimi;
			$event->puntori_id = $puntori;
			$event->save();

			//echo json_encode(array("test"=>$titulli));

			return Redirect::to_route('home');
		}

	}

?>