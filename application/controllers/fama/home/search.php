<?php
	class Fama_Home_Search_Controller extends Fama_Controller{

		public function action_search(){
			$id = Input::get("sipaspuntorit");
			if($id==0){
				return Redirect::to_route("home");
			}
			$this->layout->content = View::make('fama.home.search')
			->with("puntori_id",$id);
		}	
	}
?>