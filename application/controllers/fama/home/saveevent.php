<?php
	class Fama_Home_Saveevent_Controller extends Fama_Controller{

		public function action_saveevent(){
			$titulli = Input::get('titulli');
			$data = Input::get('data');
			$prej = Input::get('prej');
			$deri = Input::get('deri');
			$pershkrimi = Input::get('pershkrimi');
			$puntori = Input::get("puntori");

			Events::create(array(
				"title"=>$titulli,
				"start"=>date("Y-m-d ".$prej.":s",strtotime($data)),
				"end"=>date("Y-m-d ".$deri.":s",strtotime($data)),
				"description"=>$pershkrimi,
				"puntori_id"=>$puntori,
			));

			//echo json_encode(array("test"=>$titulli));

			return Redirect::to_route('home');
		}

	}

?>