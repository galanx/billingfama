<?php
	class Fama_Klientat_Kerkoklient_Controller extends Fama_Controller{

		public function action_kerkoklient(){
			$kerko = Input::get("kerko");

			$result = Klientat::where("emri","like",$kerko."%")
			->or_where("mbiemri","like",$kerko."%")
			->or_where("adresa","like",$kerko."%")
			->or_where("tel","like",$kerko."%")
			->or_where("email","like",$kerko."%")
			->or_where("id","like",$kerko."%")->get();

			$this->layout->content = View::make("fama.klientat.kerkoklient")
			->with("klientet",$result);
		}
	}
?>