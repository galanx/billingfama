<?php
	class Fama_Klientat_Shtoklient_Controller extends Fama_Controller{

		public function action_shtoklient(){
			$emri = Input::get("emri");
			$mbiemri = Input::get("mbiemri");
			$adresa = Input::get("adresa");
			$dtl = Input::get("dtl");
			$tel = Input::get("tel");
			$email = Input::get("email");

			$query = Klientat::create(array(
				"emri"=>$emri,
				"mbiemri"=>$mbiemri,
				"adresa"=>$adresa,
				"data_e_lindjes"=>date("Y-m-d",strtotime($dtl)),
				"tel"=>$tel,
				"email"=>$email,
			));

			if($query){
				return Redirect::back();
			}else{
				die("Kemi probleme me databaze, te dhenat nuk u futen :(");
			}
		}
	}
?>