<?php
	class Fama_Klientat_Index_Controller extends Fama_Controller{

		public function action_index(){
			$klientet = Klientat::order_by("id","asc")->paginate();
			$this->layout->content = View::make("fama.klientat.index")
			->with("klientet",$klientet);
		}
	}
?>