<?php
	class Fama_Kaca_Index_Controller extends Fama_Controller{

		public function action_index(){
			$this->layout->content = View::make("fama.kaca.index");
		}

		public function action_paguaj(){
			date_default_timezone_set("Europe/Tirane");

			$vlera = Input::get("vlera");
			$fatura = Input::get("fatura");
			$furnizuesi = Input::get("furnizuesi");
			$shpenzuesi = Input::get("shpenzuesi");
			$pershkrimi = Input::get("pershkrimi");

			$paguaj = Kaca::create(array(
				"vlera"=>$vlera,
				"fatura"=>$fatura,
				"furnizuesi"=>$furnizuesi,
				"shpenzuesi"=>$shpenzuesi,
				"pershkrimi"=>$pershkrimi,
				"data"=>date("Y-m-d H:i:s"),
				"data_raport"=>date("Y-m-d"),
			));

			if($paguaj){
				return Redirect::back()->with("success","Pagesa u regjistrua me sukses");
			}else{
				die("Probleme me databazen :(");
			}
			
		}
	}
?>