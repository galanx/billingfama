<?php
	class Fama_Konto_Index_Controller extends Fama_Controller{

		public function action_index(){
			$results = Users::all();
			$this->layout->content = View::make("fama.konto.index")
			->with("results",$results);
		}

		public function action_rolet(){
			$results = Roles::all();
			$this->layout->content = View::make("fama.konto.rolet")
			->with("results",$results);
		}

		public function action_info(){
			$this->layout->content = View::make("fama.konto.info");
		}

		public function action_shto(){
			$this->layout->content = View::make("fama.konto.shto");
		}

		public function action_edit($id){
			$this->layout->content = View::make("fama.konto.ndryshokonto")
			->with("id",$id);
		}

		public function action_shtorole(){
			$this->layout->content = View::make("fama.konto.shtorole");
		}

		public function action_ndryshorole($id){
			$this->layout->content = View::make("fama.konto.ndryshorole")
			->with("id",$id);
		}

		public function action_infochange(){
			$user = Input::get("user");
			$password = Input::get("password");
			$confirm = Input::get("confirm");

			if($user==""){
				return Redirect::back()
				->with("msg","Ju lutem plotësoni emrin!");
			}
			if($password==""){
				return Redirect::back()
				->with("msg","Ju lutem plotësojeni fjalëkalimin");
			}
			if($password!=$confirm){
				return Redirect::back()
				->with("msg","Fjalëkalimi duhet të konfirmohet");
			}

			$query = Users::where("id","=",Auth::user()->id)->first();

			if($query){
				$query->username = $user;
				$query->password = Hash::make($password);
				$query->save();

				return Redirect::back()
				->with("success","Te dhënat u ndryshuan me sukses!");
			}else{
				die("Problem me databaze :(");
			}
		}

		public function action_shtokonto(){
			$user = Input::get("user");
			$password = Input::get("password");
			$confirm = Input::get("confirm");
			$role = Input::get("rolet");
			$perqindja = Input::get("perqindja");

			if($user==""){
				return Redirect::back()
				->with("msg","Ju lutem plotësoni emrin!");
			}
			if(Users::where("username","=",$user)->first()){
				return Redirect::back()
				->with("msg","Konto ekziston!");
			}
			if($password==""){
				return Redirect::back()
				->with("msg","Ju lutem plotësojeni fjalëkalimin");
			}
			if($password!=$confirm){
				return Redirect::back()
				->with("msg","Fjalëkalimi duhet të konfirmohet");
			}if($role==""){
				return Redirect::back()
				->with("msg", "Roli duhet të zgjidhet!");
			}
			if($perqindja==""){
				return Redirect::back()
				->with("msg", "Perqindja e rroges duhet te ipet");
			}

			$query = Users::create(array(
					"username"=>$user,
					"password"=>Hash::make($password),
					"role_id"=>$role,
					"perqindja"=>$perqindja
				));

			if($query){
				return Redirect::back()
				->with("success","Konto ".$user." u regjistrua me sukses!");
			}
		}

		public function action_addrole(){
			$emri = Input::get("emri");
			$termin_add = Input::get("termin_add");
			$termin_edit = Input::get("termin_edit");
			$klient_add = Input::get("klient_add");
			$klient_delete = Input::get("klient_delete");
			$kaca_add = Input::get("kaca_add");
			$shitja_add = Input::get("shitja_add");
			$sherbim_add = Input::get("sherbim_add");
			$sherbim_delete = Input::get("sherbim_delete");
			$raportet_view = Input::get("raportet_view");
			$produktet = Input::get("produktet");

			if($emri==""){
				return Redirect::back()
				->with("msg","Emri duhet te plotësohet");
			}

			$query = Roles::create(array(
				"roli"=>$emri,
				"termin_add"=>$termin_add,
				"termin_edit"=>$termin_edit,
				"klient_add"=>$klient_add,
				"klient_delete"=>$klient_delete,
				"kaca_add"=>$kaca_add,
				"shitja_add"=>$shitja_add,
				"sherbim_add"=>$sherbim_add,
				"sherbim_delete"=>$sherbim_delete,
				"raportet_view"=>$raportet_view,
				"produktet"=>$produktet,
			));

			if($query){
				return Redirect::back()
				->with("success","Roli është regjistruar me sukses!");
			}else{
				die("Probleme me databaze :(");
			}
		}


		public function action_editkonto(){
			$id = Input::get("id");
			$user = Input::get("user");
			$password = Input::get("password");
			$confirm = Input::get("confirm");
			$role = Input::get("rolet");
			$perqindja = Input::get("perqindja");

			if($user==""){
				return Redirect::back()
				->with("msg","Ju lutem plotësoni emrin!");
			}
			if(Users::where("username","=",$user)->where("id","!=",$id)->first()){
				return Redirect::back()
				->with("msg","Konto ekziston!");
			}
			if($password==""){
				return Redirect::back()
				->with("msg","Ju lutem plotësojeni fjalëkalimin");
			}
			if($password!=$confirm){
				return Redirect::back()
				->with("msg","Fjalëkalimi duhet të konfirmohet");
			}if($role==""){
				return Redirect::back()
				->with("msg", "Roli duhet të zgjidhet!");
			}
			if($perqindja==""){
				return Redirect::back()
				->with("msg", "Perqindja e rroges duhet te ipet!");
			}
			$query = Users::where("id","=",$id)->first();

			if($query){
				$query->username = $user;
				$query->password = Hash::make($password);
				$query->role_id = $role;
				$query->perqindja = $perqindja;
				$query->save();

				return Redirect::back()
				->with("success","Konto u ndryshua me sukses!");
			}
		}

		public function action_editrole(){
			$id = Input::get("id");
			$emri = Input::get("emri");
			$termin_add = Input::get("termin_add");
			$termin_edit = Input::get("termin_edit");
			$klient_add = Input::get("klient_add");
			$klient_delete = Input::get("klient_delete");
			$kaca_add = Input::get("kaca_add");
			$shitja_add = Input::get("shitja_add");
			$sherbim_add = Input::get("sherbim_add");
			$sherbim_delete = Input::get("sherbim_delete");
			$raportet_view = Input::get("raportet_view");
			$produktet = Input::get("produktet");

			if($emri==""){
				return Redirect::back()
				->with("msg","Emri duhet te plotësohet");
			}

			$query = Roles::where("id","=",$id)->first();

			if($query){
				$query->roli = $emri;
				$query->termin_add = $termin_add;
				$query->termin_edit = $termin_edit;
				$query->klient_add = $klient_add;
				$query->klient_delete = $klient_delete;
				$query->kaca_add = $kaca_add;
				$query->shitja_add = $shitja_add;
				$query->sherbim_add = $sherbim_add;
				$query->sherbim_delete = $sherbim_delete;
				$query->raportet_view = $raportet_view;
				$query->produktet = $produktet;
				$query->save();
			}

			if($query){
				return Redirect::back()
				->with("success","Roli është regjistruar me sukses!");
			}else{
				die("Probleme me databaze :(");
			}
		}

		public function action_deletekonto(){
			$checkbox = Input::get("checked");
			if($checkbox==""){
				return Redirect::back();
			}
			
			foreach($checkbox as $checked){

				if($checked==Auth::user()->id){
					return Redirect::back()
					->with("msg","Nuk mund ta fshini vetveten!");
				}
				if(Users::where("id","=",$checked)->first()->role_id==1){
					return Redirect::back()
					->with("msg","Nuk mund ta fshini Super Administratorin!");
				}
				$delete = Users::where("id","=",$checked)
				->where("id","!=",Auth::user()->id)
				->where("role_id","!=",1)->delete();
			}

			return Redirect::back();
		}

		public function action_fshijrole(){
			$checkbox = Input::get("checked");

			if($checkbox==""){
				return Redirect::back();
			}

			foreach($checkbox as $checked){

				if($checked==1){
					return Redirect::back()
					->with("msg","Nuk mund ta terminoni Super Administratorin!");
				}
				if($checked == Auth::user()->role_id){
					return Redirect::back()
					->with("msg","Nuk mund ta terminoni rolin që posedoni për momentin!");
				}
				$delete = Roles::where("id","=",$checked)
				->where("id","!=",Auth::user()->role_id)
				->where("id","!=",1)->delete();
			}

			return Redirect::back();
		}
	}
?>