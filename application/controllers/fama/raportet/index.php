<?php
	class Fama_Raportet_Index_Controller extends Fama_Controller{

		public function action_index(){
			$this->layout->content = View::make("fama.raportet.index");
		}

		public function action_view(){
			$prej = Input::get("prej");
			$deri = Input::get("deri");
			$checks = Input::get("checks");

			//Pagesat
			if($checks==1&&$prej!=""&&$deri==""){
				$result = Kaca::where("data_raport",">=",$prej)->order_by("data_raport","asc")->get();
			}
			if($checks==1&&$prej==""&&$deri!=""){
				$result = Kaca::where("data_raport","<=",$deri)->order_by("data_raport","asc")->get();
			}
			if($checks==1&&$prej!=""&&$deri!=""){
				$result = Kaca::where("data_raport",">=",$prej)->where("data_raport","<=",$deri)->order_by("data_raport","asc")->get();
			}
			if($checks==1&&$prej==""&&$deri==""){
				return Redirect::back()
				->with("msg","Ju lutem zgjidheni daten");
			}

			//Shitja
			if(Auth::user()->id==1){
				if($checks==2&&$prej!=""&&$deri==""){
					$result = Shitja::where("sherbimi",">",0)->where("data_raport",">=",$prej)->order_by("data_raport","asc")->get();
					$prod = Shitja::where("produkti",">",0)->where("data_raport",">=",$prej)->order_by("data_raport","asc")->get();
				}
				if($checks==2&&$prej==""&&$deri!=""){
					$prod = Shitja::where("produkti",">",0)->where("data_raport","<=",$deri)->order_by("data_raport","asc")->get();
					$result = Shitja::where("sherbimi",">",0)->where("data_raport","<=",$deri)->order_by("data_raport","asc")->get();
				}	
				if($checks==2&&$prej!=""&&$deri!=""){
					$result = Shitja::where("sherbimi",">",0)->where("data_raport",">=",$prej)->where("data_raport","<=",$deri)->order_by("data_raport","asc")->get();
					$prod = Shitja::where("produkti",">",0)->where("data_raport",">=",$prej)->where("data_raport","<=",$deri)->order_by("data_raport","asc")->get();
				}
				if($checks==2&&$prej==""&&$deri==""){
					return Redirect::back()
					->with("msg","Ju lutem zgjidheni daten");
				}
			}else{
				if($checks==2&&$prej!=""&&$deri==""){
					$result = Shitja::where("sherbimi",">",0)->where("puntori_id","=",Auth::user()->id)->where("data_raport",">=",$prej)->order_by("data_raport","asc")->get();
					$prod = Shitja::where("produkti",">",0)->where("puntori_id","=",Auth::user()->id)->where("data_raport",">=",$prej)->order_by("data_raport","asc")->get();
				}
				if($checks==2&&$prej==""&&$deri!=""){
					$result = Shitja::where("sherbimi",">",0)->where("puntori_id","=",Auth::user()->id)->where("data_raport","<=",$deri)->order_by("data_raport","asc")->get();
					$prod = Shitja::where("produkti",">",0)->where("puntori_id","=",Auth::user()->id)->where("data_raport","<=",$deri)->order_by("data_raport","asc")->get();
				}
				if($checks==2&&$prej!=""&&$deri!=""){
					$result = Shitja::where("sherbimi",">",0)->where("puntori_id","=",Auth::user()->id)->where("data_raport",">=",$prej)->where("data_raport","<=",$deri)->order_by("data_raport","asc")->get();
					$prod  = Shitja::where("produkti",">",0)->where("puntori_id","=",Auth::user()->id)->where("data_raport",">=",$prej)->where("data_raport","<=",$deri)->order_by("data_raport","asc")->get();
				}
				if($checks==2&&$prej==""&&$deri==""){
					return Redirect::back()
					->with("msg","Ju lutem zgjidheni daten");
				}
			}

			if($checks==3&&$prej!=""&&$deri==""){
				$result = Hyrjemalli::where("data",">=",$prej)->order_by("data","asc")->get();
			}
			if($checks==3&&$prej==""&&$deri!=""){
				$result = Hyrjemalli::where("data","<=",$deri)->order_by("data","asc")->get();
			}
			if($checks==3&&$prej!=""&&$deri!=""){
				$result = Hyrjemalli::where("data",">=",$prej)->where("data","<=",$deri)->order_by("data","asc")->get();
			}
			if($checks==3&&$prej==""&&$deri==""){
				return Redirect::back()
				->with("msg","Ju lutem zgjidheni daten");
			}

			if($checks==1){
				$this->layout->content = View::make("fama.raportet.pagesat")
				->with("result",$result);
			}
			if($checks==2){
				$this->layout->content = View::make("fama.raportet.shitja")
				->with("result",$result)->with("prods",$prod)->with("prej",$prej)->with("deri",$deri);
			}
			if($checks==3){
				$this->layout->content = View::make("fama.raportet.malli")
				->with("result",$result);
			}
		}
	}
?>