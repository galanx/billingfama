<?php
	class Fama_Shitja_Shto_Controller extends Fama_Controller{

		public function action_shtohyrje(){

			$sherbimet = Input::get("sherbimet");
			$produktet = Input::get("produktet");

			$zbrCheck = Input::get("zbritja");
			$zbritja = Input::get("vlera");
			$arsyeja = Input::get("koment");
			$furnizuesi = Input::get("furnizuesi");
			$puntori = Input::get("puntori");
			

			if(isset($sherbimet)){
				$sherbimi = Input::get("sherbimi");
				$cmimis = Input::get("cmimis");

				if($sherbimi==""){
					return Redirect::back()
					->with("error","Ju lutem zgjidheni sherbimin!");
				}
				if($cmimis==""){
					return Redirect::back()
					->with("error","Ju lutem plotesojeni cmimin!");
				}
				if($furnizuesi==""){
					return Redirect::back()
					->with("error","Ju lutem plotesojeni furnizuesin!");
				}
				if($puntori==""){
					return Redirect::back()
					->with("error","Ju lutem zgjidheni puntorin!");
				}
				if(isset($zbrCheck)&&$arsyeja==""){
					return Redirect::back()
					->with("error","Arsyeja e zbritjes duhet te plotesohet!");
				}
				$shitja = Shitja::create(array(
					"furnizuesi"=>$furnizuesi,
					"puntori_id"=>$puntori,
					"sherbimi"=>$sherbimi,
					"cmimi"=>$cmimis,
					"zbritja"=>$zbritja,
					"arsyeja"=>$arsyeja,
					"data"=>date("Y-m-d H:i:s"),
					"data_raport"=>date("Y-m-d"),
				));
			}

			if(isset($produktet)){
				$saHere = Input::get("saHere");

				for($i=0;$i<=$saHere;$i++){

					if($i==0){
						$produkti = Input::get("produkti");
						$sasia = Input::get("sasia");
						$cmimi = Input::get("cmimi");
					}else{
						$produkti = Input::get("produkti".$i);
						$sasia = Input::get("sasia".$i);
						$cmimi = Input::get("cmimi".$i);
					}

					if($produkti==""){
						return Redirect::back()
						->with("error","Ju lutem zgjidheni produktin!");
					}
					if($cmimi==""){
						return Redirect::back()
						->with("error","Ju lutem plotesojeni cmimin!");
					}
					if($furnizuesi==""){
						return Redirect::back()
						->with("error","Ju lutem plotesojeni klientin!");
					}
					if($puntori==""){
						return Redirect::back()
						->with("error","Ju lutem zgjidheni puntorin!");
					}
					if(isset($zbrCheck)&&$arsyeja==""){
						return Redirect::back()
						->with("error","Arsyeja e zbritjes duhet te plotesohet!");
					}

					if($i==0&&!isset($sherbimet)){
						$shitjap = Shitja::create(array(
							"furnizuesi"=>$furnizuesi,
							"puntori_id"=>$puntori,
							"produkti"=>$produkti,
							"sasia"=>$sasia,
							"cmimi"=>$cmimi*$sasia,
							"zbritja"=>$zbritja,
							"arsyeja"=>$arsyeja,
							"data"=>date("Y-m-d H:i:s"),
							"data_raport"=>date("Y-m-d"),
						));
					}else{
						$shitjap = Shitja::create(array(
							"furnizuesi"=>$furnizuesi,
							"puntori_id"=>$puntori,
							"produkti"=>$produkti,
							"sasia"=>$sasia,
							"cmimi"=>$cmimi*$sasia,
							"data"=>date("Y-m-d H:i:s"),
							"data_raport"=>date("Y-m-d"),
						));
					}

					$stoku = Produktet::where("id","=",$produkti)->first();
					$stoku->gjendja -= $sasia;
					$stoku->save();
				}

			}

			if(!isset($produktet)&&!isset($sherbimet)){
				return Redirect::back()
				->with("error","Ju lutem zgjidheni sherbimet ose produktet!");
			}
			
			return Redirect::back()
			->with("msg","Te hyrat u regjistruan me sukses!");
		}
	}
?>