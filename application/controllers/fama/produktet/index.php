<?php
	class Fama_Produktet_Index_Controller extends Fama_Controller{

		public function action_index(){
			$this->layout->content = View::make("fama.produktet.index");
		}

		public function action_hyrjet(){
			$this->layout->content = View::make("fama.produktet.hyrjet");
		}

		public function action_shto(){
			$produkti = Input::get("produkti");
			$cmimi = Input::get("cmimi");

			if($produkti==""||$produkti==""){
				return Redirect::back();
			}
				$shto = Produktet::create(array(
					"produkti"=>$produkti,
					"cmimi"=>$cmimi,
				));

			if($shto){
				return Redirect::back()->with("msg","Produkti u shtua me sukses!");
			}else{
				return Redirect::back()->with("error","Produkti nuk u shtua!");
			}
		}

		public function action_fshij(){
			$checked = Input::get("checked");

			if($checked==""){
				return Redirect::back();
			}

			foreach($checked as $check){
				$delete = Produktet::where("id","=",$check)->delete();
			}

			return Redirect::back();
		}

		public function action_shtohyrje(){
			$saHere = Input::get("saHere");
			$furnizuesi = Input::get("furnizuesi");
			$fatura = Input::get("fatura");
			$koment = Input::get("koment");

			for($i=0;$i<=$saHere;$i++){
				if($i==0){
					$produkti = Input::get("produkti");
					$sasia = Input::get("sasia");
					$cmimi = Input::get("cmimi");
				}else{
					$produkti = Input::get("produkti".$i);
					$sasia = Input::get("sasia".$i);
					$cmimi = Input::get("cmimi".$i);
				}


				Hyrjemalli::create(array(
					"furnizuesi"=>$furnizuesi,
					"p_id"=>$produkti,
					"sasia"=>$sasia,
					"cmimi"=>$cmimi,
					"fatura"=>$fatura,
					"koment"=>$koment,
					"data"=>date("Y-m-d"),
				));

				$p = Produktet::where("id","=",$produkti)->first();
				$p->gjendja+=$sasia;
				$p->save();
			}

			return Redirect::to_route("produktet");
		}
	}
?>