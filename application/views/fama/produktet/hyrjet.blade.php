<script>
				function validate(evt) {
			  var theEvent = evt || window.event;
			  var key = theEvent.keyCode || theEvent.which;
			  key = String.fromCharCode( key );
			  var regex = /[0-9]|\./;
			  if( !regex.test(key) ) {
			    theEvent.returnValue = false;
			    if(theEvent.preventDefault) theEvent.preventDefault();
			  }
			}

				window.onload = function(){
				document.getElementById("shitja").setAttribute("class","current");
				}
</script>
<?php $i = 0?>
@foreach(Produktet::all() as $produkt)	

	<input type="hidden" id="list{{$i}}" name="{{$produkt->id}}" value="{{$produkt->produkti}}">
	<?php $i++; ?>
@endforeach	
<input type="hidden" id="numri" value="{{$i}}"/>
@include('fama.produktet.menu')
<div id="shitja-interface">
	<div id="shitja-main">
			<div id="edit">
			<h2>Shto Hyrje</h2><br><br>			
			<p style="color:#FF6666">@if(Session::has('msg'))
			{{Session::get('msg')}}<br><br>
			@endif</p>
			{{Form::open("/produktet/shtohyrje", "POST")}}
			{{Form::label("furnizuesi","Furnizuesi: ")}}
			{{Form::text("furnizuesi")}}<br><br>
		<table style="width:auto;margin:0 auto;" id="inhere" name="">
			<tr>
				<th>Produkti</th><th>Sasia</th><th>Çmimi:</th>
				<th><a href="javascript:AddFormField('inhere','text','','','td','select'); kalkulo();" style="text-decoration:none;">
				<span style="font-weight:bold;color:green;font-size:1.3em;"> + </span></th>
			</tr>
			<tr>
				<td>
					<select name="produkti" id="produkti" onmouseover="pershkrimi(this.value);">
						<option value="" selected>--Zgjidheni Produktin--</option>
						@foreach(Produktet::all() as $produkt)
						<option value="{{$produkt->id}}">{{$produkt->produkti}}</option>
						@endforeach
					</select>
				</td>
				<td>
					{{Form::text('sasia',"",array('style'=>'width:50px;padding:0;margin:0;', 'onkeypress'=>'validate(event)',"class"=>"sasia" ,'id'=>'sasia'))}}
				</td>
				<td>
					{{Form::text('cmimi',"",array('style'=>'width:50px;padding:0;margin:0;', 'onkeypress'=>'validate(event)',"class"=>"cmimi",'id'=>'cmimi'))}}
				</td>
			</tr>
		</table><br><br>
		<span style="color:#666699;">Total: <span id="result"></span>&euro;</span><br><br><br>
		{{Form::label("fatura","Fatura: ")}}
		{{Form::text('fatura',"",array('style'=>'width:70px;padding:0;margin:0;'))}}
		<br><br>
		{{Form::label("koment","Koment (opsional): ")}}
		{{Form::textarea("koment",'',array('style'=>'width:200px; height:50px;'))}}<br><br>
		{{Form::hidden("saHere","",array("id"=>"saHere"))}}
		{{Form::submit('Shto',array('style'=>'width:50px; height:30px;'))}}
		{{Form::close()}}
		</div>
	</div>
</div>
{{HTML::script('js/kalkulo.js')}}