	<script type="text/javascript">
			function validate(evt) {
			  var theEvent = evt || window.event;
			  var key = theEvent.keyCode || theEvent.which;
			  key = String.fromCharCode( key );
			  var regex = /[0-9]|\./;
			  if( !regex.test(key) ) {
			    theEvent.returnValue = false;
			    if(theEvent.preventDefault) theEvent.preventDefault();
			  }
			}
	</script>
<?php $roli = Roles::where("id","=",Auth::user()->role_id)->first(); ?>

@if($roli->sherbim_add==1)
@include('fama.produktet.menu')
<div id="edit" style="width:280px;">
	@if(Session::has("error"))
		<p style="color:#FF6666;">{{Session::get("error")}}</p>
	@endif
	@if(Session::has("msg"))
		<p>{{Session::get("msg")}}</p>
		<br><br>
	@endif
	{{Form::open('produktet/shto','POST')}}
	{{Form::label('produkti', 'Produkti: ')}}<br>
	{{Form::text('produkti')}}	
	{{Form::label('cmimi', 'Vlera: ')}}<br>
	{{Form::text("cmimi","",array("onkeypress"=>"validate(event)","style"=>"width:60px;"))}}<br>
	{{Form::submit('Shto',array('style'=>'width:50px; height:30px; margin-bottom:-10px'))}}
	{{Form::close()}}
</div>
@endif
<table class="tabelat" style="width:500px;">
	<caption><button id="back" onclick="javascript:history.go(-1);">&lt;&lt;Mbrapa</button> Produktet me çmime.</caption>
	{{Form::open('produktet/fshij',"POST",array("id"=>"fshijProd"))}}
	<tr><th><a href="#" style="color:white;text-decoration:none;" onclick="document.getElementById('fshijProd').submit();">[X]</a></th><th>ID</th><th>Produkti</th><th>Cmimi</th><th>Gjendja</th>
	@foreach(Produktet::all() as $produkti)
	<tr>
		<td>{{Form::checkbox('checked[]',$produkti->id)}}</td>			
		<td>{{$produkti->id}}</td>
		<td>{{$produkti->produkti}}</td>
		<td>{{$produkti->cmimi}}&euro;</td>
		<td>{{$produkti->gjendja}}</td>
	</tr>
	@endforeach
	{{Form::close()}}
</table>