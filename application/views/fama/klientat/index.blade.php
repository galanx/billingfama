<?php $roli = Roles::where("id","=",Auth::user()->role_id)->first(); ?>

@if($roli->klient_add==1)
<a href="#" id="shto_klient">Shto Klient</a>
@endif
<table id="klientat" class="tabelat">
	{{Form::open('fshijklient',"POST",array("id"=>"delete"))}}
	<tr><th><a href="#" style="color:white;text-decoration:none;" onclick="document.getElementById('delete').submit();">[X]</a></th><th>ID</th><th>Emri</th><th>Mbiemri</th><th>Adresa</th><th>Data e lindjes</th><th>Tel</th><th>Email</th></tr>
	@foreach($klientet->results as $klient)
	<tr>
		@if($roli->klient_delete==1)
		<td>{{Form::checkbox('checked[]',$klient->id)}}</td>
		@else
		<td><input type="checkbox" disabled/></td>
		@endif
		<td>{{$klient->id}}</td>
		<td>{{$klient->emri}}</td>
		<td>{{$klient->mbiemri}}</td>
		<td>{{$klient->adresa}}</td>
		<td>{{date("d/m/Y",strtotime($klient->data_e_lindjes))}}</td>
		<td>{{$klient->tel}}</td>
		<td>{{$klient->email}}</td>
	</tr>
	@endforeach
	{{Form::close();}}	
</table>
{{$klientet->links()}}
		<form id="shtoklient_form" name="shtoklient_form" action="/shtoklient" method="POST" style="display:none;width:200px;">
			<div style="float:right;"><a href="#" id="close_klient" >X</a></div><br><br>
			<h2 id="titulli">Shto Klient</h2><br><br>
			<input type="hidden" id="dataEdit" name="dataEdit" />
			<input type="hidden" id="id" name="id" />
			<label>Emri: </label><br><input type="text" id="emri" name="emri" /><br /><br />
			<label>Mbiemri: </label><br><input type="text" id="mbiemri" name="mbiemri" /><br /><br />
			<label>Adresa: </label><br><input type="text" id="adresa" name="adresa" /><br /><br />
			<label>Data e lindjes: </label><br><input type="text" id="dtl" name="dtl" /></textarea><br /><br />
			<label>Tel: </label><br><input type="text" id="tel" name="tel" /><br /><br />
			<label>Email: </label><br><input type="text" id="email" name="email" /><br /><br />
			<input id="ruaj" type="submit" value="Shto" />
		</form>
					<div id="kerkoKlient" style="margin-top:50px;">
						<form name="data" action="/kerkoklient", method="post">
							<input name="kerko" type="text"/>
							<input type="submit" value="kerko" id="submitButton"/>
						</form>
					</div>
	<script>
		$(document).ready(function(){
			$("#dtl").datepicker();
		});
	</script>	
