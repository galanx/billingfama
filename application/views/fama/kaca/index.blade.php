<div id="edit">
	<script type="text/javascript">
			function validate(evt) {
			  var theEvent = evt || window.event;
			  var key = theEvent.keyCode || theEvent.which;
			  key = String.fromCharCode( key );
			  var regex = /[0-9]|\./;
			  if( !regex.test(key) ) {
			    theEvent.returnValue = false;
			    if(theEvent.preventDefault) theEvent.preventDefault();
			  }
			}
	</script>		
	<h2>Pagesa nga kaca</h2><br><br>
	<p>@if(Session::has('success'))
		{{Session::get('success')}}
		<br><br>
	@endif<p>
	{{Form::open('/kaca/paguaj', 'POST')}}
	{{Form::label('vlera','Vlera: ',array("id"=>"vleraLabel"))}}<br>
	{{Form::text('vlera', "",array('style'=>'width:100px;', 'onkeypress'=>'validate(event)', "id"=>"vlera"))}}
	{{Form::label('fatura','Fatura: ',array( "id"=>"faturaLabel"))}}<br>
	{{Form::text('fatura',"",array('style'=>'width:100px;',"id"=>"fatura"))}}
	{{Form::label('furnizuesi','Furnizuesi: ',array("id"=>"furnizuesiLabel"))}}<br>
	{{Form::text('furnizuesi','', array( "id"=>"furnizuesi"))}}
	{{Form::label('shpenzuesi','Shpenzuesi: ',array("id"=>"shpenzuesiLabel"))}}<br>
	{{Form::text('shpenzuesi','', array( "id"=>"shpenzuesi"))}}
	{{Form::label('pershkrimi','Përshkrimi: ',array( "id"=>"pershkrimiLabel"))}}<br>
	{{Form::textarea('pershkrimi','',array("id"=>"pershkrimi"))}}
	<br>
	{{Form::submit('Paguaj',array('style'=>'width:50px; height:30px;'))}}
	{{Form::close()}}<br><br>
	<?php 
		$shitja = Shitja::sum('cmimi')-Shitja::sum('zbritja');
		$pagesat = Kaca::sum("vlera");
		$totali = $shitja - $pagesat;	
	 ?>
	<p>Gjendja e kaces: {{number_format($totali,2)}}&euro;</p>
</div>