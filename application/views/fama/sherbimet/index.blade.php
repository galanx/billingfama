	<script type="text/javascript">
			function validate(evt) {
			  var theEvent = evt || window.event;
			  var key = theEvent.keyCode || theEvent.which;
			  key = String.fromCharCode( key );
			  var regex = /[0-9]|\./;
			  if( !regex.test(key) ) {
			    theEvent.returnValue = false;
			    if(theEvent.preventDefault) theEvent.preventDefault();
			  }
			}
	</script>
<?php $roli = Roles::where("id","=",Auth::user()->role_id)->first(); ?>

@if($roli->sherbim_add==1)
<div id="edit" style="width:280px;">
	@if(Session::has("error"))
		<p style="color:#FF6666;">{{Session::get("error")}}</p>
	@endif
	@if(Session::has("msg"))
		<p>{{Session::get("msg")}}</p>
		<br><br>
	@endif
	{{Form::open('sherbimet/shto','POST')}}
	{{Form::label('sherbimi', 'Sherbimi: ')}}<br>
	{{Form::text('sherbimi')}}	
	{{Form::label('cmimi', 'Vlera: ')}}<br>
	{{Form::text("cmimi","",array("onkeypress"=>"validate(event)","style"=>"width:60px;"))}}<br>
	{{Form::submit('Shto',array('style'=>'width:50px; height:30px; margin-bottom:-10px'))}}
	{{Form::close()}}
</div>
@endif
<table class="tabelat" style="width:500px;">
	<caption><button id="back" onclick="javascript:history.go(-1);">&lt;&lt;Mbrapa</button> Sherbimet me çmime.</caption>
	{{Form::open('sherbimet/fshij',"POST",array("id"=>"fshijSherb"))}}
	<tr><th><a href="#" style="color:white;text-decoration:none;" onclick="document.getElementById('fshijSherb').submit();">[X]</a></th><th>ID</th><th>Sherbimi</th><th>Cmimi</th>
	@foreach(Sherbimet::all() as $sherbim)
		<tr>
		@if($roli->sherbim_delete==1)
		<td>{{Form::checkbox('checked[]',$sherbim->id)}}</td>	
		@else
		<td><input type="checkbox" disabled/></td>
		@endif	
		
		<td>{{$sherbim->id}}</td>
		<td>{{$sherbim->sherbimi}}</td>
		<td>{{$sherbim->cmimi}}&euro;</td>
		</tr>
	@endforeach
	{{Form::close()}}
</table>

