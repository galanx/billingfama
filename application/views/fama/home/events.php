<?php
	$jsonArray = array();
	foreach(Events::all() as $event){
		$puntori=Users::where("id","=",$event->puntori_id)->first();
		$buildjson = array(
			'id'=>$event->id,
			'title' => $event->title, 
			'start' => $event->start,
			'end' => $event->end, 
			'allday' => $event->allDay,
			'description'=>$event->description,
			'puntori'=>$event->puntori_id,
			'puntori_name'=>$puntori->username
		);
		array_push($jsonArray, $buildjson);
	}

	echo json_encode($jsonArray);
?>