		<?php $roli = Roles::where("id","=",Auth::user()->role_id)->first(); ?>
		<script>
			var BASE = "<?php echo URL::base(); ?>";
			var datetime = "";
			$(document).ready(function() {

				$('#calendar').fullCalendar({
				  	axisFormat: 'HH:mm',
				  	timeFormat: { 
					month: 'HH:mm', 
					agendaWeek: 'HH:mm { - HH:mm}' 
					},
					disableResizing: true,
					eventSources: [
	                    {	
	                        url: BASE+'/events',
	                        timeFormat: 'H(:mm)',
	                        type: 'POST',
	                        editable: true,
	                        allDayDefault:false,
	                        color: '#65a9d7',    // an option!
	                        textColor: '#3c3d3d',  // an option!
	                    }, 	                                       
	                ],	

					eventDrop: function(event,dayDelta,minuteDelta,allDay,revertFunc) {

				  		if (!confirm("Jeni të sigurt për këtë ndryshim?")) {
				            revertFunc();
				        }else{
				        	$.post(BASE+'/changeevent',{days:dayDelta,min:minuteDelta,idi:event.id},function(result){
				        	//alert(result.idi+" Ditet: "+result.days+" Min: "+result.min);
							}, "json");
				        }
				    },

				    <?php if($roli->termin_add==1){ ?>
				    dayClick: function( date, allDay, jsEvent, view ) { 
				    	
				    	$("#overlay_form").fadeIn(1000);
						positionPopup();
						$("#title").text("Termini: "+$.fullCalendar.formatDate(date,"dd/MM/yyyy"));
						$("#data").val($.fullCalendar.formatDate(date,"yyyy-MM-dd"));
						$("#prej").val($.fullCalendar.formatDate(date,"HH:mm"));
						$('#prej').timepicker();
						$('#deri').timepicker();
				    },
				    <?php } ?>
				    <?php if($roli->termin_edit==1){ ?>
				    eventClick: function( event, jsEvent, view ) { 
				    	$("#edit_form").fadeIn(1000);
						editpositionPopup();
						$("#titleEdit").text("Termini: "+$.fullCalendar.formatDate(event.start,"dd/MM/yyyy"));
						$("#dataEdit").val($.fullCalendar.formatDate(event.start,"yyyy-MM-dd"));
						$("#prejEdit").val($.fullCalendar.formatDate(event.start,"HH:mm"));
						$("#deriEdit").val($.fullCalendar.formatDate(event.end,"HH:mm"));
						$('#prejEdit').timepicker();
						$('#deriEdit').timepicker();
						$('#selectedEdit').val(event.puntori);
						$('#selectedEdit').text(event.puntori_name);
						$("#titulliEdit").val(event.title);
						$("#pershkrimiEdit").val(event.description);
						$("#id").val(event.id);
						$("#deleteEvent").attr("href", "/deleteevent/"+event.id);
						$("#deleteEvent").click(function(){
							if(!confirm("Jeni të sigurt që dëshironi t'a fshini këtë termin?")){
								return false;
							}
						});
				    },
				    <?php } ?>

				    eventRender: function(event, element, view) {
				    	if(view.name == "agendaDay"){
				    		element.find('.fc-event-title').append(" - Te: "+event.puntori_name+" - Pershkrimi: "+event.description);
				    	}
					    
				    } 
				  });
			});	
		</script>
		{{Form::open("/terminesearch","POST", array("name"=>"change", "id"=>"change"))}}
		<div id="terminesearch" id="sp">
			<select name="sipaspuntorit" id="sp" onchange="document.getElementById('change').submit();">
				<option value="0">Të gjitha terminet</option>
				@foreach(Users::all() as $user)
					<option value="{{$user->id}}">{{$user->username}}</option>
				@endforeach
			</select>
		</div>
		{{Form::close()}}
		
		<div id="calendar"></div>
		<br />
			<form id="overlay_form" name="overlay_form"  action="/saveevent" method="POST" style="display:none">
			<div style="float:right;"><a href="#" id="close" >X</a></div><br><br>
			<h2 id="title"></h2><br><br>
			<input type="hidden" id="data" name="data" />
			<label>Prej: </label><br><input type="text" id="prej" name="prej" /><br /><br />
			<label>Deri: </label><br><input type="text" id="deri" name="deri" /><br /><br />
			<label>Emri i klientit: </label><br><input type="text" id="titulli" name="titulli" /><br /><br />
			<label>Puntori: </label><br>
			<select id="puntori" name="puntori" />
				@foreach(Users::all() as $user)
				<option value="{{$user->id}}">{{$user->username}}</option>
				@endforeach
			</select>
			<br /><br />
			<label>Pershkrimi: </label><br><textarea id="pershkrimi" name="pershkrimi" /></textarea><br /><br />
			<input id="ruaj" type="submit" value="Ruaj" />
			
		</form>
		<br />
		<form id="edit_form" name="edit_form"  action="/editevent" method="POST" style="display:none">
			<div style="float:right;"><a href="#" id="closeEdit" >X</a></div><br><br>
			<h2 id="titleEdit"></h2><br><br>
			<input type="hidden" id="dataEdit" name="dataEdit" />
			<input type="hidden" id="id" name="id" />
			<label>Prej: </label><br><input type="text" id="prejEdit" name="prejEdit" /><br /><br />
			<label>Deri: </label><br><input type="text" id="deriEdit" name="deriEdit" /><br /><br />
			<label>Emri i klientit: </label><br><input type="text" id="titulliEdit" name="titulliEdit" /><br /><br />
			<label>Puntori: </label><br>
			<select id="puntoriEdit" name="puntoriEdit" />
				<option id="selectedEdit" selected></option>
				@foreach(Users::all() as $user)
				<option value="{{$user->id}}">{{$user->username}}</option>
				@endforeach
			</select><br><br>
			<label>Pershkrimi: </label><br><textarea id="pershkrimiEdit" name="pershkrimiEdit" /></textarea><br /><br />
			<input id="ruaj" type="submit" value="Ndrysho" />
			<div style="float:right;"><a href="" id="deleteEvent" style="text-align:right;">Fshij</a></div><br><br>
		</form>
		<script>
				var BASE = "<?php echo URL::base(); ?>";
				$("#sp").change(function(){
					$.post(BASE+'/events',{data:$("#sp").val()},function(result){
						$("#cmimis").val(result.cmimi);
					}, "json");	
				});
		</script>