<div id="edit">
	<script type="text/javascript">
			function validate(evt) {
			  var theEvent = evt || window.event;
			  var key = theEvent.keyCode || theEvent.which;
			  key = String.fromCharCode( key );
			  var regex = /[0-9]|\./;
			  if( !regex.test(key) ) {
			    theEvent.returnValue = false;
			    if(theEvent.preventDefault) theEvent.preventDefault();
			  }
			}

			function display_zbritja(){
				if(document.getElementById("zbritja").checked){
					document.getElementById("vleraLabel").setAttribute("class","visible");
					document.getElementById("vlera").setAttribute("class","visible");
					document.getElementById("zbrKomentLabel").setAttribute("class","visible");
					document.getElementById("zbrKoment").setAttribute("class","visible");
				}else{
					document.getElementById("vleraLabel").setAttribute("class","hidden");
					document.getElementById("vlera").setAttribute("class","hidden");
					document.getElementById("zbrKomentLabel").setAttribute("class","hidden");
					document.getElementById("zbrKoment").setAttribute("class","hidden");
				}
			}

			function display_produktet(){
				if(document.getElementById("produktet").checked){
					document.getElementById("prods").setAttribute("class","visible");
				}else{
					document.getElementById("prods").setAttribute("class","hidden");
				}
			}

			function display_sherbimet(){
				if(document.getElementById("sherbimet").checked){
					document.getElementById("sherbs").setAttribute("class","visible");
				}else{
					document.getElementById("sherbs").setAttribute("class","hidden");
				}
			}
	</script>		
	<h2>Shitje</h2><br><br>
	@if(Session::has("error"))
		<p style="color:#FF6666;">{{Session::get("error")}}</p><br><br>
	@endif
	@if(Session::has("msg"))
		<p>{{Session::get("msg")}}</p>
		<br><br>
	@endif
	{{Form::open('/shitja/shtohyrje', 'POST')}}

	<!--Produktet-->
	<p><label>Produktet</label><input name ="produktet", id="produktet" type="checkbox" onclick="javascript:display_produktet();"/></p>
					<div id="prods" class="hidden">
							<?php $i = 0?>
					@foreach(Produktet::all() as $produkt)	

						<input type="hidden" id="list{{$i}}" name="{{$produkt->id}}" value="{{$produkt->produkti}}">
						<?php $i++; ?>
					@endforeach	
					<input type="hidden" id="numri" value="{{$i}}"/>		
								<p style="color:#FF6666">@if(Session::has('msg'))
								{{Session::get('msg')}}<br><br>
								@endif</p>
							<table style="width:auto;margin:0 auto;" id="inhere" name="">
								<tr>
									<th>Produkti</th><th>Sasia</th><th>Çmimi:</th>
									<th><a href="javascript:AddFormField('inhere','text','','','td','select'); kalkulo();" style="text-decoration:none;">
									<span style="font-weight:bold;color:green;font-size:1.3em;"> + </span></th>
								</tr>
								<tr>
									<td>
										<select name="produkti" id="produkti" onmouseover="pershkrimi(this.value);">
											<option value="" selected>--Zgjidheni Produktin--</option>
											@foreach(Produktet::all() as $produkt)
											<option value="{{$produkt->id}}">{{$produkt->produkti}}</option>
											@endforeach
										</select>
									</td>
									<td>
										{{Form::text('sasia',"",array('style'=>'width:50px;padding:0;margin:0;', 'onkeypress'=>'validate(event)',"class"=>"sasia" ,'id'=>'sasia'))}}
									</td>
									<td>
										{{Form::text('cmimi',"",array('style'=>'width:50px;padding:0;margin:0;', 'onkeypress'=>'validate(event)',"class"=>"cmimi",'id'=>'cmimi'))}}
									</td>
								</tr>
							</table><br><br>
							<span style="color:#666699;">Total: <span id="result"></span>&euro;</span><br><br><br>
							{{Form::hidden("saHere","",array("id"=>"saHere"))}}

					{{HTML::script('js/kalkulo.js')}}
				</div>	



	<!--Sherbimet-->

	<p><label>Sherbimet</label><input name ="sherbimet", id="sherbimet" type="checkbox" onclick="javascript:display_sherbimet();"/></p>
	<div id="sherbs" class="hidden">
		<select name="sherbimi", id="sherbimi">
			<option value="" selected>--Zjidheni sherbimin--</option>
			@foreach(Sherbimet::all() as $sherbimet)
				<option value="{{$sherbimet->id}}">{{$sherbimet->sherbimi}}</option>
			@endforeach
		</select><br><br>
		{{Form::label('cmimis','Cmimi: ',array("id"=>"cmimiLabel"))}}<br>
		{{Form::text('cmimis', "",array('style'=>'width:100px;', 'onkeypress'=>'validate(event)', "id"=>"cmimis"))}}<br>
	</div>	


	{{Form::label('furnizuesi','Klienti: ')}}<br>
	{{Form::text('furnizuesi')}}<br>
	<select name="puntori", id="puntori">
		<option value="" selected>--Puntori--</option>
		@foreach(Users::all() as $user)
			<option value="{{$user->id}}">{{$user->username}}</option>
		@endforeach
	</select><br><br>
	<p><label>Zbritje</label><input name ="zbritja", id="zbritja" type="checkbox" onclick="javascript:display_zbritja();"/></p>
	{{Form::label('vlera','Vlera e zbritur: ',array('class'=>'hidden', 'id'=>'vleraLabel'))}}
	<p>{{Form::text('vlera',"",array('style'=>'width:100px;', 'onkeypress'=>'validate(event)','class'=>'hidden','id'=>'vlera'))}}</p>
	{{Form::label('koment','Arsyeja e zbritjes (e detyrueshme): ',array('class'=>'hidden', 'id'=>'zbrKomentLabel'))}}
	<p>{{Form::textarea('koment','',array('class'=>'hidden', 'id'=>'zbrKoment', 'style'=>'width:200px;height:50px;margin-bottom:20px;'))}}</p>
	{{Form::submit('Shto',array('style'=>'width:50px; height:30px;'))}}
	{{Form::close()}}<br><br>
	<?php 
		$shitja = Shitja::sum('cmimi')-Shitja::sum('zbritja');
		$pagesat = Kaca::sum("vlera");
		$totali = $shitja - $pagesat;	
	 ?>
	<p>Gjendja e kaces: {{number_format($totali,2)}}&euro;</p>
</div>
<script>
		var BASE = "<?php echo URL::base(); ?>";
		$("#sherbimi").change(function(){
			$.post(BASE+'/ajax',{data:$("#sherbimi").val()},function(result){
				$("#cmimis").val(result.cmimi);
			}, "json");	
		});
</script>