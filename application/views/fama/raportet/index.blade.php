<div id="edit">
	<?php date_default_timezone_set("Europe/Tirane") ?>
	<?php 
		if(Auth::user()->id==1){
			$pagesat = Kaca::where("data",">",date("Y-m-d H:i:s",strtotime("- 12 hour")))->sum("vlera");
			$shitja = Shitja::where("data",">",date("Y-m-d H:i:s",strtotime("- 12 hour")))->sum("cmimi")-Shitja::where("data",">",date("Y-m-d H:i:s",strtotime("- 12 hour")))->sum("zbritja"); 
		}else{
			$pagesat = Kaca::where("data",">",date("Y-m-d H:i:s",strtotime("- 12 hour")))->sum("vlera");
			$shitja = Shitja::where("puntori_id","=",Auth::user()->id)->where("data",">",date("Y-m-d H:i:s",strtotime("- 12 hour")))->sum("cmimi")-Shitja::where("data",">",date("Y-m-d H:i:s",strtotime("- 12 hour")))->sum("zbritja"); 
		}
			
		$totali = $shitja-$pagesat;
	?>
	@if(Auth::user()->id==1)
		<h2>Pazari ditorë: {{number_format($totali,2)}}&euro;</h2>
	@else
		<h2>Pazari ditorë (personal): {{number_format($totali,2)}}&euro;</h2>
	@endif
	<br><br>
	@if(Session::has("msg"))
		<p style="color:#FF6666">{{Session::get("msg")}}</p><br><br>
	@endif
	{{Form::open('raportet/view','POST')}}
	<select name="checks">
		@if(Auth::user()->id==1)
			<option value="1">Shpenzimet</option>
			<option value="2">Të hyrat</option>
			<option value="3">Të hyrat e mallit</option>
		@else
			<option value="2">Të hyrat (personale)</option>
		@endif
	</select><br>
	<br>
	{{Form::label("prej","Prej: ")}}
	{{Form::date("prej")}}<br>
	{{Form::label("deri","Deri: ")}}
	{{Form::date("deri")}}<br><br>
	{{Form::submit('Shfleto',array('style'=>'width:50px; height:30px;'))}}
	{{Form::close()}}
</div>