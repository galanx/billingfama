<table class="tabelat"><caption><button id="back" onclick="javascript:history.go(-1);">&lt;&lt;Mbrapa</button> Sherbimet.</caption>
	<?php $shtotali=0; 
		$ptotali = 0;	
	?>
	<tr><th>Klienti</th><th>Sherbimi</th><th>Cmimi</th><th>Puntori</th><th>Zbritja</th><th>Pershkrimi</th><th>Data</th></tr>
	@foreach($result as $shitja)
	<tr>
		<td>{{$shitja->furnizuesi}}</td>
		<td>{{Sherbimet::where("id","=",$shitja->sherbimi)->first()?Sherbimet::where("id","=",$shitja->sherbimi)->first()->sherbimi:""}}</td>
		<td>{{$shitja->cmimi==0?"":$shitja->cmimi."&euro;"}}</td>
		<td>{{Users::where("id","=",$shitja->puntori_id)->first()?Users::where("id","=",$shitja->puntori_id)->first()->username:""}}</td>
		<td>{{$shitja->zbritja==0?"":$shitja->zbritja."&euro;"}}</td>
		<td>{{$shitja->arsyeja}}</td>
		<td>{{date("d/m/Y H:i:s",strtotime($shitja->data))}}</td>
		<?php $shtotali += $shitja->cmimi-$shitja->zbritja; ?>
	</tr>
	@endforeach
	<tr><th></th><th></th><th>Gjithsej: {{$shtotali==0?"":$shtotali."&euro;"}}</th><th></th><th></th><th></th><th></th></tr>
</table>

<table class="tabelat"><caption>Produktet.</caption>
	<tr><th>Klienti</th><th>Produkti</th><th>Sasia</th><th>Cmimi total</th><th>Puntori</th><th>Zbritja</th><th>Pershkrimi</th><th>Data</th></tr>
	@foreach($prods as $prod)
	<tr>
		<td>{{$prod->furnizuesi}}</td>
		<td>{{Produktet::where("id","=",$prod->produkti)->first()?Produktet::where("id","=",$prod->produkti)->first()->produkti:""}}</td>
		<td>{{$prod->sasia}}</td>		
		<td>{{$prod->cmimi==0?"":$prod->cmimi."&euro;"}}</td>
		<td>{{Users::where("id","=",$prod->puntori_id)->first()?Users::where("id","=",$prod->puntori_id)->first()->username:""}}</td>
		<td>{{$prod->zbritja==0?"":$prod->zbritja."&euro;"}}</td>
		<td>{{$prod->arsyeja}}</td>
		<td>{{date("d/m/Y H:i:s",strtotime($prod->data))}}</td>
		<?php $ptotali += $prod->cmimi-$prod->zbritja; ?>
	</tr>
	@endforeach
	<tr><th></th><th></th><th>Gjithsej: {{$ptotali==0?"":$ptotali."&euro;"}}</th><th></th><th><th></th></th><th></th><th></th></tr>
</table>

<table class="tabelat" style="width:300px;">
	<tr><th>Gjithsej sherbime</th><th>Gjithsej produkte</th><th>Total</th></tr>
	<tr><td>{{number_format($shtotali,2)}}&euro;</td><td>{{number_format($ptotali,2)}}&euro;</td><td>{{number_format($shtotali+$ptotali,2)}}&euro;</td></tr>
</table>


<table class="tabelat"><caption>Rrogat.</caption>
	<tr><th>Puntori</th><th>Fitimet</th><th>Perqindja</th><th>Rroga</th></tr>
	@foreach(Users::where("role_id",">",1)->get() as $puntori)
		<?php 
			$fitimet = 0;
			if($prej!=""&&$deri==""){
				$shitja = Shitja::where("puntori_id","=",$puntori->id)->where("data_raport",">=",$prej)->get();
			}
			if($prej==""&&$deri!=""){
				$shitja = Shitja::where("puntori_id","=",$puntori->id)->where("data_raport","<=",$deri)->get();
			}
			if($prej!=""&&$deri!=""){
				$shitja = Shitja::where("puntori_id","=",$puntori->id)->where("data_raport",">=",$prej)->where("data_raport","<=",$deri)->get();
			}

			if($shitja){
				foreach($shitja as $shitje){
					$fitimet += $shitje->cmimi-$shitje->zbritja;
				}
			}else{
				$fitimet = 0;
			}
		?>
		<tr>
			<td>{{$puntori->username}}</td>
			<td>{{$fitimet}}&euro;</td>
			<td>{{$puntori->perqindja}}&#37;</td>
			<td>{{($puntori->perqindja/100)*$fitimet}}&euro;</td>
		</tr>
	@endforeach