<table class="tabelat">
	<?php $totali=0; ?>
	<tr><th>Vlera</th><th>Fatura</th><th>Furnizuesi</th><th>Shpenzuesi</th><th>Pershkrimi</th><th>Data</th></tr>
	@foreach($result as $pagesa)
	<tr>
		<td>{{$pagesa->vlera?$pagesa->vlera."&euro;":""}}</td>
		<td>{{$pagesa->fatura}}</td>
		<td>{{$pagesa->furnizuesi}}</td>
		<td>{{$pagesa->shpenzuesi}}</td>
		<td>{{$pagesa->pershkrimi}}</td>
		<td>{{date("d/m/Y H:i:s",strtotime($pagesa->data))}}</td>
		<?php $totali += $pagesa->vlera; ?>
	</tr>
	@endforeach
	<tr><th>Gjithsej: {{$totali==0?"":$totali."&euro;"}}</th><th></th><th></th><th></th><th></th><th></th></tr>
</table>