<table class="tabelat">
	<?php $totali=0; ?>
	<tr><th>Furnizuesi</th><th>Produkti</th><th>Cmimi</th><th>Sasia</th><th>Fatura</th><th>Koment</th><th>Data</th></tr>
	@foreach($result as $produkti)
	<tr>
		<td>{{$produkti->furnizuesi}}</td>
		<td>{{Produktet::where("id","=",$produkti->p_id)->first()?Produktet::where("id","=",$produkti->p_id)->first()->produkti:""}}</td>
		<td>{{$produkti->cmimi==0?"":$produkti->cmimi."&euro;"}}</td>
		<td>{{$produkti->sasia}}</td>
		<td>{{$produkti->fatura}}</td>
		<td>{{$produkti->koment}}</td>
		<td>{{date("d/m/Y",strtotime($produkti->data))}}</td>
		<?php $totali += $produkti->cmimi*$produkti->sasia; ?>
	</tr>
	@endforeach
	<tr><th></th><th></th><th>Gjithsej: {{$totali==0?"":$totali."&euro;"}}</th><th></th><th></th><th></th><th></th></tr>
</table>