<!doctype html>
<html>
	<head>
		<meta charset='utf-8'>
		<title>Fama</title>	
		<link rel="shortcut icon" href="<?php echo URL::to('favicon.ico'); ?>">	
		{{HTML::style('css/style.css')}}
		{{HTML::style('css/theme.css')}}
		{{HTML::style('css/fullcalendar.css')}}
		{{HTML::style('css/popupform.css')}}
		{{HTML::style('css/fullcalendar.print.css')}}
		{{HTML::style('css/jquery-ui.css')}}
		{{HTML::script('js/jquery.js')}}
		{{HTML::script('js/jquery-ui.js')}}
		{{HTML::script('js/datetimepicker.js')}}
		{{HTML::script('js/qtip.js')}}
		{{HTML::script('js/popupform.js')}}
		{{HTML::script('js/jquery.custom.min.js')}}
		{{HTML::script('js/fullcalendar.js')}}
		{{HTML::script('js/gcal.js')}}
		{{HTML::script('js/addform.js')}}
		{{HTML::script('js/menu-highlight.js')}}
	</head>
	<body>
		<?php $roli = Roles::where("id","=",Auth::user()->role_id)->first(); ?>
		<div id="wrapper">
			<div id="header-container">
				<div id="header">
					<div id="logo"><a href="/">{{HTML::image("img/logo.png")}}</a></div>
					<div id="top-icons">
						@if(Auth::user()->role_id==1)
						<a href="/konto">{{HTML::image("img/users.png")}}</a>
						@endif
						<a href="/logout">{{HTML::image("img/logout.png")}}</a>
					</div>
					<div id="menu">
						<div id="hidden-menu-button">
							=
						</div>
						<ul id="menu-top">
							<li><a href="/klientet" style="border-lefT:1px solid #666699;">Klientët</a></li>
							@if($roli->kaca_add==1)
							<li><a href="/kaca">Shpenzimet</a></li>
							@endif
							@if($roli->shitja_add==1)
							<li><a href="/shitja">Të hyrat</a></li>
							@endif
							@if($roli->sherbim_add==1||$roli->sherbim_delete==1)
							<li><a href="/sherbimet">Sherbimet</a></li>
							@endif
							@if($roli->produktet==1)
							<li><a href="/produktet">Produktet</a></li>
							@endif
							@if($roli->raportet_view==1)
							<li><a href="/raportet">Raportet</a></li>
							@endif
						</ul>
					</div>
				</div>
			</div>
			<script>
				$(document).ready(function(){
					$("#hidden-menu-button").click(function(){
						$("#menu-top").addClass("hiddenmenu");
						$("#menu-top").fadeToggle();
					});
				});
			</script>
			<div id="sub-wrapper">

