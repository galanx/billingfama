@include('fama.konto.aside')
	<script type="text/javascript">
			function validate(evt) {
			  var theEvent = evt || window.event;
			  var key = theEvent.keyCode || theEvent.which;
			  key = String.fromCharCode( key );
			  var regex = /[0-9]|\./;
			  if( !regex.test(key) ) {
			    theEvent.returnValue = false;
			    if(theEvent.preventDefault) theEvent.preventDefault();
			  }
			}
		</script>	
<div id="user-main" style="width:750px;float:right;margin-right:200px;">
	<div id="edit">
		<h1>Shto Konto</h1><br><br>
		<p style="color:#FF6666">@if(Session::has('msg'))
		{{Session::get('msg')}}<br><br>
		@endif<p>
		<p>@if(Session::has('success'))
		{{Session::get('success')}}<br><br>
		@endif<p>
		{{Form::open("konto/shtokonto","POST")}}
		{{Form::label("user","Emri")}}<br>
		{{Form::text("user")}}<br><br>
		{{Form::label("password", "Fjalëkalimi")}}<br>
		{{Form::password("password")}}<br><br>
		{{Form::label("confirm", "Konfirmo Fjalëkalimin")}}<br>
		{{Form::password("confirm")}}<br><br>
		<select name="rolet">
			<option value="" selected>--Zgjidheni Rolin--</option>
			@foreach(Roles::where("id","!=",1)->get() as $role)
			<option value="{{$role->id}}">{{$role->roli}}</option>
			@endforeach
		</select><br><br>
		{{Form::label("perqindja", "Perqindja e rroges")}}<br>
		{{Form::text("perqindja","",array('style'=>'width:50px;padding:0;margin:0;', 'onkeypress'=>'validate(event)'))}}&#37;<br><br>
		{{Form::submit('Shto',array('style'=>'width:50px; height:30px;'))}}
		{{Form::close()}}
	</div>
</div>