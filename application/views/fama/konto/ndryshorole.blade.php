@include('fama.konto.aside')
<?php $roli = Roles::where("id","=",$id)->first(); ?>
<div id="user-main" style="width:750px;float:right;margin-right:200px;">
	<div id="edit" style="width:250px;">
		<h1>Ndrysho Role</h1><br><br>
		<p style="color:#FF6666">@if(Session::has('msg'))
		{{Session::get('msg')}}<br><br>
		@endif<p>
		<p>@if(Session::has('success'))
		{{Session::get('success')}}<br><br>
		@endif<p>
		{{Form::open("konto/editrole","POST",array("style"=>"width:100%;min-height:420px;"))}}
		{{Form::label("emri","Emri i rolit")}}<br>
		{{Form::text("emri",$roli->roli)}}<br><br>
		<div id="left" style="width:100px;height:100%;float:left;">
			<label for="termin_add">Shto Termin </label>
			@if($roli->termin_add==1)
				<input value="1" type="checkbox" id="termin_add" name="termin_add" checked/>
			@else
				<input value="1" type="checkbox" id="termin_add" name="termin_add"/>
			@endif	
			<br>
			<label for="termin_edit">Ndrysho Termin </label>
			@if($roli->termin_edit==1)
				<input value="1" type="checkbox" id="termin_edit" name="termin_edit" checked/>
			@else
				<input value="1" type="checkbox" id="termin_edit" name="termin_edit"/>
			@endif	
			<br>
			<label for="klient_add">Shto Klient </label>
			@if($roli->klient_add==1)
				<input value="1" type="checkbox" id="klient_add" name="klient_add" checked/>
			@else
				<input value="1" type="checkbox" id="klient_add" name="klient_add"/>
			@endif	
			<br>
			<label for="klient_delete">Fshij Klient </label>
			@if($roli->klient_delete==1)
				<input value="1" type="checkbox" id="klient_delete" name="klient_delete" checked/>
			@else
				<input value="1" type="checkbox" id="klient_delete" name="klient_delete"/>
			@endif	
			<br>
			<label for="kaca_add">Shto Shpenzim </label>
			@if($roli->kaca_add==1)
				<input value="1" type="checkbox" id="kaca_add" name="kaca_add" checked/>
			@else
				<input value="1" type="checkbox" id="kaca_add" name="kaca_add"/>
			@endif	
			<br>
		</div>
		<div id="middle" style="width:100px;height:100%;float:left;">
			<label for="shitja_add">Shto Hyrje </label>
			@if($roli->shitja_add==1)
				<input value="1" type="checkbox" id="shitja_add" name="shitja_add" checked/>
			@else
				<input value="1" type="checkbox" id="shitja_add" name="shitja_add"/>
			@endif	
			<br>
			<label for="sherbim_add">Shto Sherb. </label>
			@if($roli->sherbim_add==1)
				<input value="1" type="checkbox" id="sherbim_add" name="sherbim_add" checked/>
			@else
				<input value="1" type="checkbox" id="sherbim_add" name="sherbim_add"/>
			@endif	
			<br>
			<label for="sherbim_delete">Fshij Sherb. </label>
			@if($roli->sherbim_delete==1)
				<input value="1" type="checkbox" id="sherbim_delete" name="sherbim_delete" checked/>
			@else
				<input value="1" type="checkbox" id="sherbim_delete" name="sherbim_delete"/>
			@endif	
			<br>
			<label for="raportet_view">Raportet </label>
			@if($roli->raportet_view==1)
				<input value="1" type="checkbox" id="raportet_view" name="raportet_view" checked/>
			@else
				<input value="1" type="checkbox" id="raportet_view" name="raportet_view"/>
			@endif	
			<br>
			<label for="produktet">Produktet </label>
			@if($roli->produktet==1)
				<input value="1" type="checkbox" id="produktet" name="produktet" checked/>
			@else
				<input value="1" type="checkbox" id="produktet" name="produktet"/>
			@endif	
			<br>
		</div>
		<div id="right" style="width:100px;height:100%;float:left;"></div>
		<br>
		{{Form::hidden("id",$roli->id)}}	
		{{Form::submit('Ndrysho',array('style'=>'width:50px; height:30px;display:block; clear:both;margin:0 auto;'))}}
		{{Form::close()}}
	</div>
</div>