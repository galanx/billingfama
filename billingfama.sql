-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 23, 2013 at 11:31 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `billingfama`
--

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  `description` varchar(500) NOT NULL,
  `allDay` tinyint(1) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `url` varchar(500) NOT NULL,
  `className` varchar(500) NOT NULL,
  `editable` tinyint(1) NOT NULL,
  `source` varchar(500) NOT NULL,
  `color` varchar(500) NOT NULL,
  `backgroundColor` varchar(500) NOT NULL,
  `borderColor` varchar(500) NOT NULL,
  `textColor` varchar(500) NOT NULL,
  `puntori_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `title`, `description`, `allDay`, `start`, `end`, `url`, `className`, `editable`, `source`, `color`, `backgroundColor`, `borderColor`, `textColor`, `puntori_id`) VALUES
(4, 'Albert Feka', 'Prerje e flokeve dhe fenerim', 0, '2013-04-22 09:00:00', '2013-04-22 10:30:00', '', '', 0, '', '', '', '', '', 1),
(6, 'Diamant Haziri', 'Fenerim i flokeve te gjata', 0, '2013-04-22 09:00:00', '2013-04-22 10:30:00', '', '', 0, '', '', '', '', '', 3);

-- --------------------------------------------------------

--
-- Table structure for table `hyrjet_e_mallit`
--

CREATE TABLE IF NOT EXISTS `hyrjet_e_mallit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `furnizuesi` varchar(200) NOT NULL,
  `p_id` int(11) NOT NULL,
  `sasia` int(11) NOT NULL,
  `cmimi` double NOT NULL,
  `fatura` varchar(20) NOT NULL,
  `koment` varchar(500) NOT NULL,
  `data` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `hyrjet_e_mallit`
--

INSERT INTO `hyrjet_e_mallit` (`id`, `furnizuesi`, `p_id`, `sasia`, `cmimi`, `fatura`, `koment`, `data`) VALUES
(8, 'Fama', 1, 4, 10, '213', 'sadas', '2013-04-22'),
(9, 'Fama', 3, 5, 10, '213', 'sadas', '2013-04-22'),
(10, 'Fama', 4, 2, 1.5, '213', 'sadas', '2013-04-22');

-- --------------------------------------------------------

--
-- Table structure for table `kaca`
--

CREATE TABLE IF NOT EXISTS `kaca` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vlera` double NOT NULL,
  `fatura` varchar(20) NOT NULL,
  `furnizuesi` varchar(60) NOT NULL,
  `shpenzuesi` varchar(60) NOT NULL,
  `pershkrimi` varchar(500) NOT NULL,
  `data` datetime NOT NULL,
  `data_raport` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `kaca`
--

INSERT INTO `kaca` (`id`, `vlera`, `fatura`, `furnizuesi`, `shpenzuesi`, `pershkrimi`, `data`, `data_raport`) VALUES
(1, 13, '124', 'Furnizuesi', 'Shpenzuesi', 'torte me molle', '2013-04-17 12:25:50', '0000-00-00'),
(2, 7, '34', 'Alberti', 'Shpenzuesi', '', '2013-04-17 13:59:04', '0000-00-00'),
(3, 14, '14a', 'Albert Feka', 'Albert Feka', '', '2013-04-18 16:25:42', '2013-04-18');

-- --------------------------------------------------------

--
-- Table structure for table `klientat`
--

CREATE TABLE IF NOT EXISTS `klientat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emri` varchar(100) NOT NULL,
  `mbiemri` varchar(100) NOT NULL,
  `adresa` varchar(500) NOT NULL,
  `data_e_lindjes` date NOT NULL,
  `tel` varchar(20) NOT NULL,
  `email` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `klientat`
--

INSERT INTO `klientat` (`id`, `emri`, `mbiemri`, `adresa`, `data_e_lindjes`, `tel`, `email`) VALUES
(8, 'Albert', 'Feka', 'Prroji i njelmt', '1989-11-15', '090 2039 0239', 'galanx@facebook.com'),
(9, 'Ylli', 'Zeka', '28 Nentori', '1973-12-25', '090 2039 0239', 'ylli@negenet.com'),
(10, 'Diamant', 'Haziri', 'Bregu i diellit', '1992-06-02', '090 2039 0239', 'diamanti@negenet.com');

-- --------------------------------------------------------

--
-- Table structure for table `produktet`
--

CREATE TABLE IF NOT EXISTS `produktet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `produkti` varchar(200) NOT NULL,
  `cmimi` double NOT NULL,
  `gjendja` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `produktet`
--

INSERT INTO `produktet` (`id`, `produkti`, `cmimi`, `gjendja`) VALUES
(1, 'Parfim', 15, 15),
(3, 'Oriflame', 40, 8),
(4, 'Carmine', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roli` varchar(200) NOT NULL,
  `termin_add` tinyint(1) NOT NULL,
  `termin_edit` tinyint(1) NOT NULL,
  `klient_add` tinyint(1) NOT NULL,
  `klient_delete` tinyint(1) NOT NULL,
  `kaca_add` tinyint(1) NOT NULL,
  `shitja_add` tinyint(1) NOT NULL,
  `sherbim_add` tinyint(1) NOT NULL,
  `sherbim_delete` tinyint(1) NOT NULL,
  `raportet_view` tinyint(1) NOT NULL,
  `produktet` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `roli`, `termin_add`, `termin_edit`, `klient_add`, `klient_delete`, `kaca_add`, `shitja_add`, `sherbim_add`, `sherbim_delete`, `raportet_view`, `produktet`) VALUES
(1, 'Super Administrator', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(2, 'Administrator', 0, 0, 0, 0, 0, 0, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sherbimet`
--

CREATE TABLE IF NOT EXISTS `sherbimet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sherbimi` varchar(200) NOT NULL,
  `cmimi` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `sherbimet`
--

INSERT INTO `sherbimet` (`id`, `sherbimi`, `cmimi`) VALUES
(4, 'Prerje', 7),
(5, 'Fenerimi', 5),
(6, 'Fenerimi i flokeve te gjata', 10),
(7, 'Maska e flokeve 15 min', 20),
(8, 'Rregullimi i flokeve (frizure)', 15),
(9, 'Rregullimi i flokeve ', 20);

-- --------------------------------------------------------

--
-- Table structure for table `shitja`
--

CREATE TABLE IF NOT EXISTS `shitja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `furnizuesi` varchar(60) NOT NULL,
  `puntori_id` int(11) NOT NULL,
  `sherbimi` int(11) NOT NULL,
  `produkti` int(11) NOT NULL,
  `sasia` int(11) NOT NULL,
  `cmimi` double NOT NULL,
  `zbritja` double NOT NULL,
  `arsyeja` varchar(500) NOT NULL,
  `data` datetime NOT NULL,
  `data_raport` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `shitja`
--

INSERT INTO `shitja` (`id`, `furnizuesi`, `puntori_id`, `sherbimi`, `produkti`, `sasia`, `cmimi`, `zbritja`, `arsyeja`, `data`, `data_raport`) VALUES
(21, 'Albert Feka', 3, 4, 0, 0, 7, 3, 'klient i rregullt', '2013-04-23 09:28:19', '2013-04-23'),
(22, 'Albert Feka', 3, 0, 1, 3, 30, 0, '', '2013-04-23 09:28:19', '2013-04-23'),
(23, 'Albert Feka', 3, 0, 4, 2, 3, 0, '', '2013-04-23 09:28:19', '2013-04-23'),
(24, 'Albert Feka', 1, 0, 3, 1, 20, 4, 'yes', '2013-04-23 09:29:19', '2013-04-23');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role_id`) VALUES
(1, 'admin', '$2a$08$p5t5YZON7u3X5s9I9l3bgugRdsT2Z4uK5psKgiLHsGkhg4Os0MSCa', 1),
(3, 'galanx', '$2a$08$8Dyp3wOfUyfzF624vtLjhe/21BRRHBsNhS0meNb/77BVlj.72hLqG', 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
